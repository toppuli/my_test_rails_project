FROM ruby:2.5.3
RUN apt-get update -yqq
RUN apt-get install -yqq --no-install-recommends nodejs curl
WORKDIR /usr/src/app
RUN gem install bundler
COPY Gemfile* /usr/src/app/
RUN bundle install
EXPOSE 3000
COPY . /usr/src/app/
